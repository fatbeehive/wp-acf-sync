<?php

if (!function_exists('FBH_WP_ACF_Sync_Command')) {
    function FBH_WP_ACF_Sync_Command()
    {   
        // get ACF groups
        $groups = acf_get_field_groups();
        // make sync array to collect items
        $sync   = [];

        if (empty($groups)) {
            return WP_CLI::success("No ACF Sync required");
        }

        // check for changes
        foreach ($groups as $group) {
            $local = acf_maybe_get( $group, 'local' );
			$modified = acf_maybe_get( $group, 'modified' );
			$private = acf_maybe_get( $group, 'private' );

            if ($local !== 'json' || $private) {
                // do nothing
            } elseif (!$group['ID']) {
                $sync[$group['key']] = $group;
            } elseif( $modified && $modified > get_post_modified_time('U', true, $group['ID']) ) {
                $sync[$group['key']] = $group;
            }
        }

        if (empty($sync)) {
            return WP_CLI::success("No ACF Sync required");
        }

        // if items to sync
        if (!empty($sync)) {
            $files = acf_get_local_json_files();
            foreach ($sync as $key => $field_group) {
                $local_field_group = json_decode( file_get_contents( $files[ $key ] ), true );
				$local_field_group['ID'] = $field_group['ID'];
				$result = acf_import_field_group( $local_field_group );
            }
            WP_CLI::success('ACF has Syncd');
        }
    }
}

if (defined('WP_CLI') && WP_CLI) {
    WP_CLI::add_command('acf sync', 'FBH_WP_ACF_Sync_Command');
}
