
# A WP CLI command to synchronize ACF fields 

##  For ACF 5.7 or lower 

Install "WP ACF Sync" via the Composer package manager:
```bash
composer require fatbeehive/wp-acf-sync "^1.0"
```

##  For ACF 5.8 or higher 

Install "WP ACF Sync" via the Composer package manager:
```bash
composer require fatbeehive/wp-acf-sync "^2.0"
```

##  Usage 

Synchronize the fields using:
```bash
wp acf sync
```
